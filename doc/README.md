# Gestion du site du grand manger

## Structure de fichiers

Ce site, ici, n'est pas vraiment un backoffice hein :) C'est un outil de dev pour poser du code, et, quand c'est des pages HTML direct, ça permet aussi de les héberger et de les publier « gratos » sur une URL (une adresse IP).

Donc sur la page principale de l'outil https://framagit.org/le-grand-manger/le-grand-manger.frama.io on peut voir la liste des fichiers, et naviguer dedans.

Il faut aller dans le répertoire public pour accéder aux fichiers qui font la partie visible du site.


![](20240530203427934.png)

La page suivante affiche plusieurs fichier et répertoire.

![](20240530203643967.png)

Le fichier `index.html` est LE fichier `HTML` de la page d'accueil. Il contient la structuration de la page et le contenu (texte et lien vers les images).

Le fichier `style.css` contient des règles de présentations (couleur, font, taille, ...)

Le repertoire `/images` regroupe l'ensemble des images utilisé par le site.

Le sous répertoire `/association` regroupe un fichier `index.html` qui correspond à ce que l'on voit sur l'adresse https://legrandmanger.bzh/association/ et les deux fichiers PDF présent sont proposé en lien de téléchargement dans cette page.

## Modification

![](20240530204104029.png)

Lorsque l'on click sur un fichier (dans la capture d'écran ci-dessus, c'est le fichier `index.html` qui est dans le répertoire `public`), on peut visualiser le code source de la page.

Dans cette capture d'écran, on vois une partie entête, et le début du `<body>`. C'est dans ce dernier que ce trouve le contenu de la page.

Si vous n'êtes pas familier avec HTML, vous pouvez peut-être commencer par lire la documentation proposer par la communauté Mozilla [Commencer avec le HTML](https://developer.mozilla.org/fr/docs/Learn/HTML/Introduction_to_HTML/Getting_started).

Pour passer en modification, il faut clicker sur le bouton bleu « modifier », un sous menu apparait, il sera plus simple de choisir 	« Modifier le fichier unique ».

![](20240530204416540.png)

Le fichier est maintenant affiche avec la possibilité de le modifier.

Une fois les modifications terminé, il faut se rendre en bas de page, éventuellement modifier le message de « commit » pour précier ce que l'on a réalisé, puis clicker sur le bouton « valider mes modifications ».

![](20240530204618779.png)
